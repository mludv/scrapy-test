from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy.http import Request
import json


class AtlasSpider(BaseSpider):
    name = "atlas"

    def start_requests(self):
        gene_list = [1015, 1017, 1054, 1070]
        
        for gene in gene_list:
            url = 'http://biogps.org/plugin/200/renderurl/?geneid=' + str(gene)
            request = Request(url, callback=self.parse_biogps)
            yield request

    def parse_biogps(self, response):
        data = json.loads(response.body)
        print data['success']
        if data['success']:
            print data['url']
            return Request(data['url'], callback=self.parse)

    def parse(self, response):
        hxs = HtmlXPathSelector(response)
        evidence_xpath = '//*[@id="bodyTest"]/table[1]/tr[1]/td[2]/table' \
                '[@class="margin_right"]/tr[1]/td[1]/table[1]/tr[3]/th[1]' \
                '/table[1]/tr[4]/td[1]//text()'
        evidence = hxs.select(evidence_xpath).extract()
        print evidence

