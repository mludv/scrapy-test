# Scrapy settings for extractor project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'extractor'

SPIDER_MODULES = ['extractor.spiders']
NEWSPIDER_MODULE = 'extractor.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'extractor (+http://www.yourdomain.com)'
